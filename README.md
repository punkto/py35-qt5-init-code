# Base code for a dockerized Python 3 application with pyQT5

This is a code that contains:
- Configuration files for a Docker container with the python application.
- Basic python files with main, a module and unit tests basic code.

Please read Makefile or use "make help" for a list of available commands to set up the Docker image, run it in different configurations and erase the image.
