image_name = demo_qt:main
image_hash = $(shell docker images -q $(image_name) 2> /dev/null)
work_dir = $(shell pwd)
enable_graphics_parameters = --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"

build: Dockerfile ## Builds the image
	@if test "$(image_hash)" = ""  ; then \
		docker build --tag $(image_name) . ; \
	else \
		echo "Image already exists" ; \
	fi

run: build ## Runs the aplication in a container
	docker run -ti $(enable_graphics_parameters) --rm $(image_name)

run_dev: build ## Runs the host version of the application in the container
	docker run -ti $(enable_graphics_parameters) --rm --volume=$(work_dir)/:/code:ro $(image_name)

run_term: build ## Runs a terminal in the container
	docker run -ti $(enable_graphics_parameters) --rm --volume=$(work_dir)/:/code:ro $(image_name) bash

run_python: build ## Runs Python terminal in the container
	docker run -ti $(enable_graphics_parameters) --rm --volume=$(work_dir)/:/code:ro $(image_name) python


tests: build ## Runs tests inside the container
	docker run -ti $(enable_graphics_parameters) --rm --volume=$(work_dir)/:/code:ro $(image_name) python3 -m unittest discover -s tests -v


clean: ## Removes the image
	@if test "$(image_hash)" = ""  ; then \
		echo "Image does not exist" ; \
	else \
		docker rmi $(image_name) ; \
	fi

dockerRemoveContainers: ## Removes containers with no name
	docker rm $(docker ps -a -q)

dockerRemoveUntaggedImages: ## Removes containers with no tags
	docker rmi $(docker images -a | grep "^<none>" | awk '{print $3}')

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

