FROM ubuntu:18.04

# If new python packages are needed, add them to docker_files/pip_packages.txt
# one per line.
# python3-pyqt5 is used because using pip3 for this package has given problems to me
RUN apt-get update; apt-get install python3-pip python3-pyqt5 -y
COPY docker_files/pip_packages.txt /tmp/requirements.txt
RUN python3 -m pip install --upgrade pip; pip3 install -r /tmp/requirements.txt

# Copies all the files in this directory into /code in the Docker image.
ADD ./ /code
WORKDIR /code

# Use a non root user to run the app.
RUN adduser --uid 1000 --disabled-password --gecos '' nruser && \
    chown -R nruser:nruser /code
USER nruser

# /code/main.py is the entry point of the app.
CMD ["python3", "./src/main.py"]
